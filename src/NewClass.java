import java.util.HashSet;
import java.util.Set;

/**
 * Created by Евгений on 20.02.2015.
 */
public class NewClass extends MyClass {
    public static void main(String[] args) {
        NewClass obj = new NewClass();
        Set bag_1 = new HashSet();
        Set bag_2 = new HashSet();

        bag_1.add(1);
        bag_1.add(2);
        bag_1.add(3);
        bag_1.add(4);
        bag_1.add(5);
        bag_2.add(4);
        bag_2.add(5);
        bag_2.add(9);
        bag_1.add(10);

        System.out.println("Множества:");
        System.out.print(bag_1 + "\n");//вывод в консоль данных множеств
        System.out.print(bag_2 + "\n");
        System.out.println("Проверка на равенство множеств: " + obj.equals(bag_1, bag_2) + "\n" + "Сумма множеств:"); //1. если А=В в результате true иначе false
        printSet(obj.union(bag_1, bag_2)); //Результат А ∪ В
        System.out.println();
        System.out.println("Пересечение множеств:");
        printSet(obj.intersect(bag_1, bag_2));// Результат пересечения А и В
        System.out.println("\n" + "Разность множеств:");
        printSet(obj.subtract(bag_2, bag_1)); // Разность А и В
        System.out.println("\n" + "Симетрическая разность множеств:");
        printSet(obj.symmetricSubtract(bag_1,bag_2));
    }

}

