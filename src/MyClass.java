import java.util.HashSet;
import java.util.Iterator;
import java.util.*;

/**
 * Created by Евгений on 20.02.2015.
 */

public class MyClass implements SetOperations {

    @Override
    public boolean equals(Set a, Set b) {
        return a.equals(b);
    }

    @Override
    public Set subtract(Set a, Set b) {
        Set set6 = new HashSet();
        set6.addAll(a);

        Set set7 = new HashSet();
        set7.addAll(b);
        set6.removeAll(set7);

        return set6;
    }

    @Override
     public Set union(Set a, Set b) {
        Set set3 = new HashSet();
        set3.addAll(a); //Добавление элементов множества bag1 в множество set3.

        set3.addAll(b); //Добавление элементов множества bag2 в множество set3.

        return set3;
    }

    @Override
    public Set intersect(Set a, Set b){

        Set set4 = new HashSet();

        set4.addAll(a); //Добавление элементов множества set1 в множество set4.

        Set set5 = new HashSet();

        set5.addAll(b); //Добавление элементов множества set2 в множество set5.

        set4.retainAll(set5);

        return set4;

    }

    @Override
    public Set symmetricSubtract(Set a, Set b) {
        Set set9 = new HashSet();
        set9.addAll(subtract(a,b));
        set9.addAll(subtract(b,a));
        return set9;
    }

 /*
  Метод, выводящий элементы множества в консоль.
 */

    public static Set printSet(Set set3) {

        for (Iterator iter = set3.iterator(); iter.hasNext();) {
            System.out.print(iter.next() + ", ");
        }

        return set3;
    }

}

